package testcases;


import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import org.json.simple.JSONObject;
import org.junit.Test;
import testcases.model.User;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;


public class TestCaseGorestUsers {

    private static final String API_URL = "https://gorest.co.in/public/v1/";

    private static final String TOKEN = "1db9c9b6c959682be7c96f74ca532c3cb0bd331f46b86a92602f8d319481b6f5";


    JsonPath jsonPath;

    JSONObject requestParams;

    Response response;

    @Test
    public void checkIdValues4DigitIntegerAndNoneOfThemAreNull() {
        List<Integer> userId;
        response = given().when().get(API_URL + "users");
        jsonPath = new JsonPath(response.getBody().asString());
        userId = jsonPath.getList("data.id");
        for (Integer userIdLength : userId) {
            int getUserIdLength = String.valueOf(userIdLength).length();
            assertEquals(getUserIdLength, 4);
            assertNotNull(getUserIdLength);
        }
    }

    @Test
    public void createUserWithPost() {
        User createdUser = createUser();
        createRequestParam(createdUser);

        response = given()
                .headers("Authorization", "Bearer " + TOKEN)
                .contentType("application/json")
                .body(requestParams)
                .when()
                .post(API_URL + "users")
                .then()
                .body("data.name",equalTo(createdUser.getName()))
                .body("data.email",equalTo(createdUser.getEmail()))
                .body("data.gender",equalTo(createdUser.getGender()))
                .body("data.status",equalTo(createdUser.getStatus()))
                .statusCode(201)
                .extract()
                .response();
    }

    @Test
    public void sameEmailAddressVerify() {
        User createdUser = createUser();
        createRequestParam(createdUser);

        response = given()
                .headers("Authorization", "Bearer " + TOKEN)
                .contentType("application/json")
                .body(requestParams)
                .when()
                .post(API_URL + "users")
                .then()
                .statusCode(422)
                .log()
                .body()
                .extract()
                .response();
    }

    private void createRequestParam(User createdUser) {
        requestParams = new JSONObject();
        requestParams.put("email", createdUser.getEmail());
        requestParams.put("name", createdUser.getName());
        requestParams.put("gender", createdUser.getGender());
        requestParams.put("status", createdUser.getStatus());
    }

    private User createUser() {
        User user = new User();
        user.setEmail("example2@example.com");
        user.setName("test");
        user.setGender("male");
        user.setStatus("active");

        return user;
    }

}
