package com.uzn.utilities;

import java.io.File;
import java.time.Instant;

public class FileDownloadUtil {

    public static boolean isFileDownload(String expectedFileName, String fileExtension, int timeout) {
        String folderName = System.getProperty("user.home") + File.separator + "Downloads";

        File[] listOfFiles;

        String fileName;

        boolean fileDownload = false;

        long startTime = Instant.now().toEpochMilli();

        long waitTime = startTime + timeout;

        while (Instant.now().toEpochMilli() < waitTime) {

            listOfFiles = new File(folderName).listFiles();

            for (File file : listOfFiles) {
                fileName = file.getName().toLowerCase();

                if (fileName.contains(expectedFileName.toLowerCase()) &&
                        fileName.contains(fileExtension.toLowerCase())
                        && !fileName.contains("crdownload")
                        && file.lastModified() > startTime) {
                    fileDownload = true;
                    break;
                }
            }
            if (fileDownload) {
                break;
            }
        }
        return fileDownload;
    }

}
