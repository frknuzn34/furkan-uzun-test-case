package com.uzn.base;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.uzn.utilities.ExtentManager;

public class Page {

    public static WebDriver driver;

    public static Logger log = Logger.getLogger("devpinoyLogger");
    public ExtentReports rep = ExtentManager.getInstance();

    public static ExtentTest test;

    public static void initConfiguration() {

        if (Constants.browser.equals("firefox")) {
            driver = new FirefoxDriver();
            log.debug("Launching Firefox");

        } else if (Constants.browser.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver",
                    System.getProperty("user.dir") + "/src/test/resources/executables/chromedriver");

            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("profile.default_content_setting_values.notifications", 2);
            prefs.put("credentials_enable_service", false);
            prefs.put("profile.password_manager_enabled", false);
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("useAutomationExtension", false);
            options.setExperimentalOption("prefs", prefs);
            options.addArguments("--disable-extensions");
            options.addArguments("--disable-infobars");
            options.addArguments("lang=en-GB");

            driver = new ChromeDriver(options);
            log.debug("Launching Chrome");

        } else if (Constants.browser.equals("ie")) {

            System.setProperty("webdriver.ie.driver",
                    System.getProperty("user.dir") + "\\src\\test\\resources\\executables\\IEDriver.exe");

            driver = new InternetExplorerDriver();
            log.debug("Launching IE");
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Constants.implicitwait, TimeUnit.SECONDS);


    }

    public static void quitBrowser() {

        driver.quit();

    }

}
