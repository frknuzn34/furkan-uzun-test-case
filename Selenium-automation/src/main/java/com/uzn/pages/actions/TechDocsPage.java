package com.uzn.pages.actions;

import com.uzn.base.Constants;
import com.uzn.utilities.FileDownloadUtil;
import com.uzn.utilities.ScreenshotUtil;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.uzn.base.Page;
import com.uzn.pages.locators.TechdocsPageLocators;

import java.util.Iterator;
import java.util.Set;

import static org.testng.AssertJUnit.assertTrue;

public class TechDocsPage extends Page {

    public static final String CURRENT_LINK = "https://www.lelynet.com/_layouts/15/document/TechDocHandler.aspx?name=D-S006VT_-.pdf&mode=view";

    public static final String DOWNLOADED_FILE_NAME = "D-S006VT_-";
    public TechdocsPageLocators techdocsPageLocators;


    public TechDocsPage() {
        driver.get(Constants.testSiteUrl + "/techdocs");
        this.techdocsPageLocators = new TechdocsPageLocators();
        AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 10);
        PageFactory.initElements(factory, this.techdocsPageLocators);
    }


    public void verifyDocumentOpened() {
        techdocsPageLocators.acceptCookies.click();
        techdocsPageLocators.dropdownArrow.click();
        techdocsPageLocators.productNameInput.sendKeys("LUNA EUR");
        techdocsPageLocators.productNameInput.sendKeys(Keys.ENTER);
        techdocsPageLocators.viewDocumentLink.click();

        String firstPage = handlePage();
        assert driver.getCurrentUrl().equals(CURRENT_LINK);
        driver.switchTo().window(firstPage);
        techdocsPageLocators.downloadDocumentLink.click();
        assertTrue(FileDownloadUtil.isFileDownload(DOWNLOADED_FILE_NAME, "pdf", 5000));

    }

    private static String handlePage() {
        String firstPage = driver.getWindowHandle();
        Set s = driver.getWindowHandles();

        Iterator ite = s.iterator();

        while (ite.hasNext()) {
            String pdfPage = ite.next().toString();
            if (!pdfPage.contains(firstPage)) {
                driver.switchTo().window(pdfPage);
            }
        }
        return firstPage;
    }

}
