package com.uzn.pages.actions;

import com.uzn.base.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.uzn.base.Page;
import com.uzn.pages.locators.SearchPageLocators;

import java.util.List;

import static org.testng.AssertJUnit.assertTrue;

public class HomePage extends Page {

    public static final String SEARCH_TEXT = "happy";
    public SearchPageLocators searchLocators;


    public HomePage() {
        this.searchLocators = new SearchPageLocators();
        driver.get(Constants.testSiteUrl + "/en");
        AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 10);
        PageFactory.initElements(factory, this.searchLocators);
    }

    public HomePage clickSearchButton() {
        searchLocators.acceptCookies.click();
        searchLocators.searchIcon.click();
        searchLocators.searchInputArea.sendKeys(SEARCH_TEXT);
        searchLocators.searchButton.click();

        List<WebElement> liList = searchLocators.searchResults.findElements
                (By.cssSelector("p.item-description"));

        for (WebElement webElement : liList) {
            assertTrue(webElement.getText().toLowerCase().contains(SEARCH_TEXT));
        }
        return this;
    }

}
