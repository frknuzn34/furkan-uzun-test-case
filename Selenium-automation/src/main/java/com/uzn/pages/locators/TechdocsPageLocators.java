package com.uzn.pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TechdocsPageLocators {

    @FindBy(xpath = "//body/span[1]/span[1]/span[1]/input[1]")
    public WebElement productNameInput;

    @FindBy(xpath = "//body/main[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[1]/span[1]/span[1]/span[1]/span[2]")
    public WebElement dropdownArrow;
    @FindBy(xpath = "//a[@id='cookienotice-button-accept']")
    public WebElement acceptCookies;

    @FindBy(xpath = "//body/main[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[1]/section[1]/footer[1]/p[2]/a[1]")
    public WebElement viewDocumentLink;


    @FindBy(xpath = "//body/main[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[1]/section[1]/footer[1]/p[1]/a[1]")
    public WebElement downloadDocumentLink;
}
