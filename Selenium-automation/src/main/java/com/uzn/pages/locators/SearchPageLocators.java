package com.uzn.pages.locators;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;

public class SearchPageLocators {

    @FindBy(xpath = "//div[contains(text(),'Search')]")
    public WebElement searchIcon;

    @FindBy(xpath = "//a[@id='cookienotice-button-accept']")
    public WebElement acceptCookies;

    @FindBy(xpath = "//input[@id='global-search']")
    public WebElement searchInputArea;

    @FindBy(xpath = "//button[contains(text(),'Search')]")
    public WebElement searchButton;


    @FindBy(xpath = "//body/main[1]/div[2]/div[1]/section[1]/ul[1]")
    public WebElement searchResults;

    @FindBy(css = "#flight-returning-hp-flight")
    public WebElement returnFlight;

    @FindBy(css = "#wizard-tabs")
    public WebElement space;

    @FindBy(xpath = "//div[@class='menu-bar gcw-travel-selector-wrapper']//button[@class='trigger-utility menu-trigger btn-utility btn-secondary dropdown-toggle theme-standard pin-left menu-arrow gcw-component gcw-traveler-amount-select gcw-component-initialized']")
    public WebElement travellers;


    @FindBy(xpath = "/html[1]/body[1]/meso-native-marquee[1]/section[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[2]/div[2]/section[1]/form[1]/fieldset[2]/div[1]/div[4]/div[1]/div[1]/ul[1]/li[1]/div[1]/div[1]/div[1]/div[1]/div[4]/button[1]")
    public WebElement increaseAdultCount;

    @FindBy(xpath = "//div[@class='traveler-selector-sinlge-room-data traveler-selector-room-data']//div[@class='children-wrapper']//button[@class='uitk-step-input-button uitk-step-input-plus']")
    public WebElement increaseChildrenCount;


//	@FindBy(css = "body.wrap.cf.aoa-enabled:nth-child(2) section.hero-banner-wrap.wizard-hero.mercury.hero-no-title.background.d-col-ads.core-wizard-loaded.siteid-27.theme-inverse-hero div.hero-banner.background.native-marquee div.hero-banner-gradient.native-marquee div.cols-row.hero-banner-inner:nth-child(1) div.hero-banner-box.siteId-27.cf.hideClassicDcol div.cols-row.theme-inverse-pills.wizard-tabs.cols-nested.inline-fields:nth-child(2) div.tabs-container.col section.tab-pane.cf.gcw-section-flights-tab.on:nth-child(1) form.gcw-form.flights.gcw-prepopulate-flying-from.gcw-grey-out-irrelevant-dates.flexible-shopping-form.gcw-traveler-selector.gcw-oneway-packages.gcw-lessThanNTravelers-travelerSelector.gcw-unattendedInfantInLap-travelerSelector.gcw-childAgesAllProvided-travelerSelector.gcw-allFlightsComplete.gcw-allOriginsAreDifferentFromDestinations.gcw-dynamic-leg-fields.field-border-in-high-contrast-mode div.cols-nested.ab25184-submit:nth-child(20) label.col.search-btn-col:nth-child(1) > button.btn-primary.btn-action.gcw-submit")
//	public WebElement searchButton;


//	@FindBys: Gerekli WebElement nesnelerinin verilen kriterlerin t�m�ne uymas� gerekti�inde @FindBys ek a��klamas�n� kullan�n

//	@FindAll: Gerekti�inde, WebElement nesnelerinin verilen �l��tlerden en az biriyle e�le�mesi gerekiyorsa @FindAll ek a��klamas�n� kullan�n

    @FindAll({
            @FindBy(id = "flight-age-select-1-hp-flight"),
            @FindBy(css = "#flight-age-select-1-hp-flight")
    })
    public WebElement selectChildrenAge;
}
