package com.uzn.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.uzn.base.Page;
import com.uzn.pages.actions.HomePage;

public class SearchTestCase {
	
	@BeforeTest
	public void setUp() {
		Page.initConfiguration();
	}
	
	@Test()
	public void searchWithTextTest() {
		HomePage homePage = new HomePage();
		homePage.clickSearchButton();

	}
	
	@AfterMethod
	public void tearDown() {
		if (Page.driver !=null) {
			Page.quitBrowser();
		}
	}
}
