package com.uzn.testcases;

import com.uzn.base.Page;
import com.uzn.pages.actions.TechDocsPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TechdocsTestCase {

    @BeforeTest
    public void setUp() {
        Page.initConfiguration();
    }

    @Test()
    public void techdocsViewTest() {
        TechDocsPage techDocsPage = new TechDocsPage();
        techDocsPage.verifyDocumentOpened();
    }

    @AfterMethod
    public void tearDown() {
        if (Page.driver !=null) {
            Page.quitBrowser();
        }
    }
}
